const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Pool } = require('pg');

const pool = new Pool({
  user: 'andityadimas',
  host: 'localhost',
  database: 'postgres',
  password: '',
  port: 5432,
});

const UserController = {
  async register(req, res) {
    try {
      const { name, email, password } = req.body;

      if (!name || !email || !password) {
        return res.status(400).json({ message: 'Name, email, and password are required.' });
      }

      const hashedPassword = await bcrypt.hash(password, 10);

      const query = 'INSERT INTO users (name, email, password) VALUES ($1, $2, $3)';
      await pool.query(query, [name, email, hashedPassword]);

      res.status(201).json({ message: 'User registered successfully.' });
    } catch (error) {
      console.error('Error registering user:', error);
      res.status(500).json({ message: 'Internal server error.' });
    }
  },

  async login(req, res) {
    try {
        const { email, password } = req.body;
        
        // Validate input
        if (!email || !password) {
          return res.status(400).json({ message: 'Email and password are required.' });
        }
    
        // Fetch user from database
        const query = 'SELECT * FROM users WHERE email = $1';
        const result = await pool.query(query, [email]);
        const user = result.rows[0];
    
        if (!user) {
          return res.status(401).json({ message: 'Invalid credentials.' });
        }
    
        // Compare passwords
        const passwordMatch = await bcrypt.compare(password, user.password);
    
        if (!passwordMatch) {
          return res.status(401).json({ message: 'Invalid credentials.' });
        }
    
        // Create JWT
        const token = jwt.sign({ userId: user.id }, 'your_jwt_secret', { expiresIn: '1h' });
    
        res.status(200).json({ token });
      } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ message: 'Internal server error.' });
      }
  },

  async getMe(req, res) {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, 'your_jwt_secret');
        
        const query = 'SELECT id, name, email FROM users WHERE id = $1';
        pool.query(query, [decoded.userId], (err, result) => {
          if (err) {
            console.error('Error fetching user:', err);
            return res.status(500).json({ message: 'Internal server error.' });
          }
    
          const user = result.rows[0];
          res.status(200).json(user);
        });
      } catch (error) {
        console.error('Error decoding token:', error);
        res.status(401).json({ message: 'Invalid token.' });
      }
    
  },
};

module.exports = UserController;
